/*
 Code developped in the framework of the Rey Ornament Image investigation
 project (https://ro2i.hypotheses.org/a-propos) in a partnership between
 IHRIM and Lab. Hubert Curien, both from Saint-Etienne University, funded
 by the French National Research Agency (ANR\ROIi project 2021-2025).

 This code is developped in the framework above by Thomas gautrais and
 Thierry Fournel from Laboratoire Hubert Curien.

 Permission is granted for anyone to copy, use, modify, or distribute this
 program and accompanying programs and documents for any purpose, provided
 this copyright notice is retained and prominently displayed, along with
 a note saying that the original programs are available from the ROIi
 project web or gitlab pages.
 The programs and documents are distributed without any warranty, express
 or implied.  As the programs were written for research purposes only,
 they have not been tested to the degree that would be advisable in any
 important application. All use of these programs is entirely at the
 user's own risk.

 Matlab function "addLIP", "multLIP" and "substLIP" are based on the paper
 entitled "A model for logarithmic image processing. Jourlin, M.,
 & Pinoli, J. C. (1988). Journal of microscopy, 149(1), 21-35."
 These functions are called by function "seeLogInterpQuery2DiffWithRef"
 devoted to the display of the negative and positive differences of
 a first image, query x, with respect to a reference one, y, respectively.
 The negative differences are superimposed in the Red channel whereas the
 positive ones are superimposed in the Blue channel, parameter alpha
 adjusting the interpolation between the query and the differences with
 the reference image.
*/



// converts image in color with 3 canals of uint8 elements to image in grayscale with 1 canal of float with values between 0 and 1.
function convert_RGBuint8_to_grayfloat(uint8_data) {
  var data_float = Float32Array.from(uint8_data);
  var x = new Float32Array(data_float.length / 4);
  for(var i = 0; i < x.length; i += 1) {
    x[i] = 0.299 * data_float[i*4] + 0.587 * data_float[i*4+1] + 0.114 * data_float[i*4+2];
  }
  for(var i = 0; i < x.length; i += 1) {
    x[i] /= 255.0;
  }
  return x;
}


// converts image in color with 3 canals of uint8 elements to image in color with 3 canals of float with values between 0 and 1.
function convert_RGBuint8_to_RGBfloat(uint8_data) {
  var x = Float32Array.from(uint8_data);
  for(var i = 0; i < x.length; i += 1) {
    x[i] /= 255.0;
  }
  return x;
}


// q_data and m_data are supposed to be Uint8ClampedArray objects (corresponding to the data member of ImageData objects)
function get_seeLogInterpQuery2Ref(q_data, m_data, alpha_str) {
  var x = convert_RGBuint8_to_RGBfloat(q_data);
  var y = convert_RGBuint8_to_RGBfloat(m_data);
  var z = Float32Array.from(q_data);
  var alpha = parseFloat(alpha_str);

  // equivalent of the seeLogInterpQuery2Ref matlab function
  for(var i = 0; i < x.length/4; i += 1) {

    z[4*i] = 1.0 - Math.pow(1-y[4*i+2],alpha)*Math.pow(1-x[4*i+2],1-alpha);
    z[4*i + 1] = 1.0 - Math.pow(1-y[4*i+1],alpha)*Math.pow(1-x[4*i+1],1-alpha);
    z[4*i + 2] = 1.0 - Math.pow(1-y[4*i],alpha)*Math.pow(1-x[4*i],1-alpha);
  }

  for(var i = 0; i < x.length/4; i += 1) {
    z[4*i] *= 255.0;     // red canal
    z[4*i + 1] *= 255.0; // green canal
    z[4*i + 2] *= 255.0; // blue canal
    z[4*i + 3] = 255.0;  // alpha canal
  }

  var z_uint = Uint8ClampedArray.from(z);
  return z_uint;
}


// q_data and m_data are supposed to be Uint8ClampedArray objects (corresponding to the data member of ImageData objects)
function get_seeLogInterpQuery2DiffWithRef(q_data, m_data, alpha_str) {
  var x = convert_RGBuint8_to_grayfloat(q_data);
  var y = convert_RGBuint8_to_grayfloat(m_data);
  var z = new Float32Array(q_data.length);
  var alpha = parseFloat(alpha_str);

  // equivalent of the seeLogInterpQuery2DiffWithRef matlab function
  for(var i = 0; i < x.length; i += 1) {
    var ymx = y[i] - x[i];
    var onemx_pow_onemalpha = Math.pow(1-x[i],1-alpha);
    if (ymx > 0) {
      z0 = 1.0 - (ymx/(1-x[i]));
      z2 = 1.0;
    } else {
      z0 = 1.0;
      z2 = 1.0 + (ymx/(1-y[i]));
    }
    z[i*4] = 1.0 - Math.pow(z2,alpha)*onemx_pow_onemalpha;
    z[i*4 + 1] = 1.0 - onemx_pow_onemalpha;
    z[i*4 + 2] = 1.0 - Math.pow(z0,alpha)*onemx_pow_onemalpha;
  }

  for(var i = 0; i < x.length; i += 1) {
    z[4*i] *= 255.0;     // red canal
    z[4*i + 1] *= 255.0; // green canal
    z[4*i + 2] *= 255.0; // blue canal
    z[4*i + 3] = 255.0;  // alpha canal
  }

  var z_uint = Uint8ClampedArray.from(z);
  return z_uint;
}


function promise_load_image(img_url,img) {
  return new Promise((resolve, reject) => {
    img.addEventListener('load', () => resolve(img));
    img.addEventListener('error', (err) => reject(err));
    if(img_url) {
      img.src = img_url;
    }
  });
}

function img_to_uint8clampedarray(img) {
  var canvas = document.createElement("canvas");
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext("2d", { alpha: false });
  ctx.drawImage(img, 0, 0);
  var ctx2 = canvas.getContext("2d", { alpha: false });
  img_data = ctx2.getImageData(0, 0, canvas.width, canvas.height);
  return img_data.data;
}

function update_img_from_uint8clampedarray(img, data ) {

  var canvas = document.createElement('canvas');
  canvas.width = img.width;
  canvas.height = img.height;
  var ctx = canvas.getContext('2d', { alpha: false });
  var imgData = ctx.createImageData(canvas.width, canvas.height);
  imgData.data.set(data);
  ctx.putImageData(imgData, 0, 0);
  img.src = canvas.toDataURL();
    
}

function update_LogInterpQuery2DiffWithRef(url1, url2, img, alpha) {
  var st = img.style;
  var style_str = "height: " + st["height"] + "; width: " + st["width"];
  var image1 = new Image();
  var image2 = new Image();

  var prom1 = promise_load_image(url1, image1);
  var prom2 = promise_load_image(url2, image2);

  return new Promise((resolve, reject) => {
    Promise.all([prom1,prom2]).then(images => {
      image_data1 = img_to_uint8clampedarray(images[0]);
      image_data2 = img_to_uint8clampedarray(images[1]);
      var res_data = get_seeLogInterpQuery2DiffWithRef(image_data1, image_data2, alpha);
      img.style = "";
      update_img_from_uint8clampedarray(img, res_data);
      img.style = style_str;
      resolve(1);
    });
  });
}
